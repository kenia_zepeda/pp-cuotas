package com.pp.cuotas.dto.info;

public interface InfoPersona {

	public Long getId_persona();
	public String getNombre();
	public String getApellido();
	public String getApellido_casada();
	public String getCorreo();
	public Integer getValor_documento();
}
