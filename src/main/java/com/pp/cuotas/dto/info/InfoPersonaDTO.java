package com.pp.cuotas.dto.info;

import java.time.LocalDate;

public interface InfoPersonaDTO {
	
	public String getNombre();
	public String getApellido();
	public LocalDate getFecha_nacimiento();
	
}
