package com.pp.cuotas.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pp.cuotas.dto.PersonaDTO;
import com.pp.cuotas.dto.info.InfoPersona;
import com.pp.cuotas.model.Persona;
import com.pp.cuotas.repository.IPersonaRepository;
import com.pp.cuotas.service.IPersonaService;

@Service
public class PersonaService implements IPersonaService{

	@Autowired
	private IPersonaRepository personarepository;
	
	// METODOS DE ICRUD
	
	@Override
	public Persona registrar(Persona t) {
		return this.personarepository.save(t);
	}

	@Override
	public Persona modificar(Persona t) {
		return this.personarepository.save(t);
	}

	@Override
	public void eliminar(Long id) {
		this.personarepository.deleteById(id);		
	}

	@Override
	public List<Persona> listar() {
		return this.personarepository.findAll();
	}

	@Override
	public Persona listarId(Long id) {
		return this.personarepository.findById(id).orElse(null);
	}


	@Override
	public List<InfoPersona> Personainfo(String pnom) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<PersonaDTO> Personadto(String pfecha) {
		// TODO Auto-generated method stub
		return null;
	}
	

}
