package com.pp.cuotas.service;

import java.util.List;

import com.pp.cuotas.dto.PersonaDTO;
import com.pp.cuotas.dto.info.InfoPersona;
import com.pp.cuotas.model.Persona;

public interface IPersonaService extends ICRUD<Persona>{


	// ************************************************************************************* PARA OBTENER DATOS DE LA BASE **************************************************************************

	
	//metodo para obtener nominas del mes
	public List<InfoPersona> Personainfo(String pnom);
	

	
	//metodo para obtener nominas del mes
	public List<PersonaDTO> Personadto(String pfecha);
	
	
	
	
}
