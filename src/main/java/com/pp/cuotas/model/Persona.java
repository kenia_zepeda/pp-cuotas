package com.pp.cuotas.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "INFORMACION DE TABLA PERSONA")
@Entity
@Table(name = "PERSONA")
public class Persona {

	@ApiModelProperty(notes = "OBLIGATORIO, LO GENERA EL SISTEMA")
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id_persona;

	@ApiModelProperty(notes = "OBLIGATORIO APELLIDO NOMBRE")
	@Column(name = "nombre", nullable = false, length = 64)
	private String nombre;
	
	@ApiModelProperty(notes = "OBLIGATORIO APELLIDO")
	@Column(name = "apellido", nullable = false, length = 64)
	private String apellido;
	
	@ApiModelProperty(notes = "OBLIGATORIO APELLIDO CASADA")
	@Column(name = "apellido_casada", nullable = false, length = 64)
	private String apellido_casada;
	
	@ApiModelProperty(notes = "OPCIONAL, FECHA DE NACIMIENTO")
	@Column(name = "fecha_nacimiento", nullable = true)
	@JsonSerialize(using = LocalDateSerializer.class)
	@JsonFormat(pattern = "dd/MM/yyyy")
	private LocalDate fecha_nacimiento;
	
	@ApiModelProperty(notes = "OBLIGATORIO CORREO")
	@Column(name = "correo", nullable = false, length = 64)
	private String correo;
	
	@ApiModelProperty(notes = "OBLIGATORIO VALOR DOCUMENTO")
	@Column(name = "valor_documento", nullable = false, length = 64)
	private Integer valor_documento;
	

	public Long getId_persona() {
		return id_persona;
	}

	public void setId_persona(Long id_persona) {
		this.id_persona = id_persona;
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getApellido_casada() {
		return apellido_casada;
	}

	public void setApellido_casada(String apellido_casada) {
		this.apellido_casada = apellido_casada;
	}

	public LocalDate getFecha_nacimiento() {
		return fecha_nacimiento;
	}

	public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
		this.fecha_nacimiento = fecha_nacimiento;
	}
	
	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public Integer getValor_documento() {
		return valor_documento;
	}

	public void setValor_documento(Integer valor_documento) {
		this.valor_documento = valor_documento;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id_persona == null) ? 0 : id_persona.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((apellido == null) ? 0 : apellido.hashCode());
		result = prime * result + ((apellido_casada == null) ? 0 : apellido_casada.hashCode());
		result = prime * result + ((fecha_nacimiento == null) ? 0 : fecha_nacimiento.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Persona other = (Persona) obj;
		if (id_persona == null) {
			if (other.id_persona != null)
				return false;
		} else if (!id_persona.equals(other.id_persona))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (apellido == null) {
			if (other.apellido != null)
				return false;
		} else if (!apellido.equals(other.apellido))
			return false;
		if (apellido_casada == null) {
			if (other.apellido_casada != null)
				return false;
		} else if (!apellido_casada.equals(other.nombre))
			return false;
		if (fecha_nacimiento == null) {
			if (other.fecha_nacimiento != null)
				return false;
		} else if (!fecha_nacimiento.equals(other.fecha_nacimiento))
			return false;

		return true;
	}
	
}
