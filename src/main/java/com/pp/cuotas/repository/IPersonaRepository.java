package com.pp.cuotas.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.pp.cuotas.dto.info.InfoPersona;
import com.pp.cuotas.dto.info.InfoPersonaDTO;
import com.pp.cuotas.model.Persona;

public interface IPersonaRepository extends JpaRepository<Persona, Long>{

	
	//Info of interface
	@Query(value = "select p.nombre 'nombre',\r\n" + 
			"	   p.apellido 'apellido'\r\n" + 
			"from PRUEBA p\r\n" + 
			"where p.apellido = :nom", nativeQuery = true)
	List<InfoPersona> InfoPersona(
			@Param("nom") String nom
			);
	
	
	//Info para DTO
	@Query(value = "select p.nombre 'nombre',\r\n" + 
			"	   p.apellido 'apellido',\r\n" + 
			"	   p.fecha_nacimiento 'fecha'\r\n" + 
			"from PRUEBA p\r\n" + 
			"where p.fecha_nacimiento = :pfecha", nativeQuery = true)
	List<InfoPersonaDTO> InfoDTO(
			@Param("pfecha") LocalDate pfecha
			);
	
	
}
