package com.pp.cuotas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PpCuotasApplication {

	public static void main(String[] args) {
		SpringApplication.run(PpCuotasApplication.class, args);
	}

}
